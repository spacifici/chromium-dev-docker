ARG UBUNTU_VERSION="20.04"

FROM ubuntu:$UBUNTU_VERSION

ARG UNAME=user
ARG UUID=1000
ARG UGID=${UUID}
ARG PASSWORD=password
ARG SSHD_PORT=2222

ARG SDK_VERSION="eyeo-111-dev"
ARG SDK_BASE_URL="https://gitlab.com/eyeo/adblockplus/chromium-sdk"
ARG INSTALL_DEPS_SCRIPT="install-build-deps.sh"
ARG SDK_INSTALL_DEPS_URL="${SDK_BASE_URL}/-/raw/${SDK_VERSION}/build/${INSTALL_DEPS_SCRIPT}"

ARG RIPGREP_VERSION="13.0.0"
ARG RIPGREP_DEB_FILE="ripgrep_${RIPGREP_VERSION}_amd64.deb"
ARG RIPGREP_DEB_URL="https://github.com/BurntSushi/ripgrep/releases/download/${RIPGREP_VERSION}/${RIPGREP_DEB_FILE}"

ARG FZF_VERSION="0.43.0"
ARG FZF_TGZ_FILE="fzf-${FZF_VERSION}-linux_amd64.tar.gz"
ARG FZF_TGZ_URL="https://github.com/junegunn/fzf/releases/download/${FZF_VERSION}/${FZF_TGZ_FILE}"
ARG FZF_KEYBINDINGS_FILE="key-bindings.bash"
ARG FZF_KEYBINDINGS_URL="https://raw.githubusercontent.com/junegunn/fzf/${FZF_VERSION}/shell/${FZF_KEYBINDINGS_FILE}"

ARG BAT_VERSION="0.24.0"
ARG BAT_DEB_FILE="bat_${BAT_VERSION}_amd64.deb"
ARG BAT_DEB_URL="https://github.com/sharkdp/bat/releases/download/v${BAT_VERSION}/${BAT_DEB_FILE}"

ARG DEPOT_TOOLS_REPO=" https://chromium.googlesource.com/chromium/tools/depot_tools.git"

ARG GOMA_SERVER_HOST=ekanite.goma.engflow.com
ARG GOMA_SERVER_PORT=443
ARG GOMACTL_USE_PROXY=false

ENV DEBIAN_FRONTEND=noninteractive
ENV LANG=C.UTF-8
ENV LC_ALL=C.UTF-8
ENV LANGUAGE=C.UTF-8
ENV TZ=Etc/UTC

RUN yes|unminimize -y \
&&  apt-get upgrade -y \
&&  apt-get install -y \
    build-essential \
    curl \
    git \
    git-lfs \
    locales \
    lsb-release \
    lsof \
    man \
    openssh-server \
    rsync \
    sudo \
    tzdata

COPY sshd_config /etc/ssh/sshd_config

# Install Chromium dependencies
RUN curl -LO "${SDK_INSTALL_DEPS_URL}" \
&&  chmod +x "${INSTALL_DEPS_SCRIPT}" \
&&  ./${INSTALL_DEPS_SCRIPT} --android --no-chromeos-fonts

# Install ripgrep
RUN curl -LO "${RIPGREP_DEB_URL}" \
&&  dpkg -i "${RIPGREP_DEB_FILE}" \
&&  rm -f "${RIPGREP_DEB_FILE}"

# Install fzf
RUN curl -LO "${FZF_TGZ_URL}" \
&&  tar xf "${FZF_TGZ_FILE}" \
&&  mv fzf /usr/bin/ \
&&  rm -f "${FZF_TGZ_FILE}"

# Install bat
RUN curl -LO "${BAT_DEB_URL}" \
&&  dpkg -i "${BAT_DEB_FILE}" \
&&  rm -f "${BAT_DEB_FILE}"

# Create a regular user
RUN groupadd -g ${UGID} "${UNAME}" \
&&  useradd -m -g ${UGID} -G users -u ${UUID} -s /bin/bash "${UNAME}" \
&&  echo "${UNAME}:${PASSWORD}"|chpasswd \
&&  passwd --expire "${UNAME}" \
&&  echo "${UNAME} ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/90-${UNAME}-all

COPY bashrc_extras.sh /tmp/bashrc_extras.sh

USER ${UNAME}
RUN cd "${HOME}" \
&&  mkdir -p "opt" \
&&  git clone "https://chromium.googlesource.com/chromium/tools/depot_tools.git" "opt/depot_tools" \
&&  echo "export GOMA_SERVER_HOST=\"${GOMA_SERVER_HOST}\"" >> .bashrc \
&&  echo "export GOMA_SERVER_PORT=\"${GOMA_SERVER_PORT}\"" >> .bashrc \
&&  echo "export GOMACTL_USE_PROXY=\"${GOMACTL_USE_PROXY}\"" >> .bashrc \
&&  echo 'export PATH="${HOME}/opt/depot_tools:$PATH"' >> .bashrc \
&&  cat /tmp/bashrc_extras.sh >> .bashrc \
&&  git lfs install \
&&  mkdir -p ".fzf" \
&&  curl -OL "${FZF_KEYBINDINGS_URL}" \
&&  mv "${FZF_KEYBINDINGS_FILE}" .fzf/

USER root
COPY entrypoint.sh /root/entrypoint.sh
RUN chmod +x /root/entrypoint.sh \
&&  echo "USER_NAME=\"${UNAME}\"" > /root/entrypoint.config \
&&  echo "USER_GROUP=\"${UNAME}\"" >> /root/entrypoint.config \
&&  echo "USER_ID=\"${UUID}\"" >> /root/entrypoint.config \
&&  echo "USER_GID=\"${UGID}\"" >> /root/entrypoint.config \
&&  echo "SSHD_PORT=${SSHD_PORT}" >> /root/entrypoint.config

RUN mkdir -p /var/run/sshd \
&&  chmod 0700 /var/run/sshd

ENTRYPOINT /root/entrypoint.sh
