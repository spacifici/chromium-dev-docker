#!/bin/bash

set -x

source /root/entrypoint.config
declare -r USER_HOME="`eval "echo ~${USER_NAME}"`"
declare -r NEW_HOME="/data/home"

# Move user home to data, so we can use a volume.
mkdir -p "${NEW_HOME}"
chown ${USER_NAME}:${USER_GROUP} "${NEW_HOME}"
rsync -au "${USER_HOME}/" "${NEW_HOME}"
usermod -d "${NEW_HOME}" "${USER_NAME}"

# Make sure /var/run/user/<uid> and has the correct permissions. It can be
# a volume too, so we can keep goma cache.
runuser="/var/run/user/${USER_ID}"; 
mkdir -p "${runuser}"
chown ${USER_ID}:${USER_GID} "${runuser}"
chmod 0700 "${runuser}"

/usr/sbin/sshd -D -p ${SSHD_PORT} &

wait -n
