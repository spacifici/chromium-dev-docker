## Build the image

```sh
docker build -t devenv:latest \
  --build-arg UUID=`id -u` \
  --build-arg UGID=`id -g` .
```


## Run the container

### docker run
```sh
docker run -d --rm \
  --restart unless-stopped \
  --network host \
  --name devenv \
  -v "<path_to_persistent_home>:/data/home" \
  -v "<path_to_goma_tmpdir>:/var/run/user/<uuid>" \
  -v "<path_to_src>:/src" \
  devenv:latest
```

### docker compose

TBD

## Build arguments

* **UNAME**: user name inside the container (default: user)
* **UUID**: desired user id inside the container, should be the same id your
  user has on the host (default: 1000)
* **UGID**: desired group id inside the container (default: same as UUID)
* **PASSWORD**: default user password. It will be reset at first login (default:
  password)
* **SSHD_PORT**: ssh server port (default: 2222)
* **SDK_VERSION**: eyeo Chromium SDK version to be used to install compilation
  dependencies (default: eyeo-101-dev)
* **GOMA_SERVER_HOST**: goma server hostname
* **GOMA_SERVER_PORT**: goma server port (default: 443)

