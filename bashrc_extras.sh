# Ensuring we are logged in goma and the latter is running
LOGGED_IN="$(goma_auth info 2>/dev/null | grep 'Ready to use Goma')"

if [ -z "${LOGGED_IN}" ]; then
	echo "Running goma_auth..."
	goma_auth login && goma_ctl restart 2>/dev/null
else
	goma_ctl ensure_start 2>/dev/null
fi

# Add fzf key bindings
source "$HOME/.fzf/key-bindings.bash"

# fzf magic
export FZF_DEFAULT_OPTS="--preview 'if [ -d "{}" ]; then ls --color "{}"; else bat --color=always -r :100 "{}"; fi'"

# git aliases
alias ga='git add'
alias gb='git branch'
alias gc!='git commit -v --amend'
alias gc='git commit -v'
alias gca!='git commit -v -a --amend'
alias gca='git commit -v -a'
alias gcb='git checkout -b'
alias gco='git checkout'
alias gd='git diff'
alias glo='git log --oneline --decorate'
alias gst='git status'
